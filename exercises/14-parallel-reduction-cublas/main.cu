// ###
// ###
// ### Practical Course: GPU Programming in Computer Vision
// ###
// ###
// ### Technical University Munich, Computer Vision Group
// ### Summer Semester 2015, September 7 - October 6
// ###
// ###
// ### Thomas Moellenhoff, Robert Maier, Caner Hazirbas
// ###
// ###
// ###
// ### THIS FILE IS SUPPOSED TO REMAIN UNCHANGED
// ###
// ###


#include "aux.h"
#include <iostream>
#include "cublas_v2.h"

using namespace std;

int main(int argc, char **argv)
{
	int arraySize = 100000;
	float* a = (float*) malloc (sizeof(float) * arraySize);

	float* d_a;
	cudaMalloc((void**) &d_a, sizeof(float) * arraySize);

	for (int i=0; i<arraySize; i++)
	    a[i]= 0.8f;

	cudaMemcpy(d_a, a, sizeof(float) * arraySize, cudaMemcpyHostToDevice);

	cublasStatus_t ret;  
	cublasHandle_t handle;
	ret = cublasCreate(&handle);

	float* cb_result = (float*) malloc (sizeof(float));

	ret = cublasSasum(handle, arraySize, d_a, 1 , cb_result);

	
	cout << "Sum : " << *cb_result <<endl;

	cublasDestroy(handle);
    
    return 0;
}



