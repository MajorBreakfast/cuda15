# 1.
`nvcc -o squareArray squareArray.cu && ./squareArray`

# 2.
`nvcc -o addArrays addArrays.cu && ./addArrays`

# 3.
`nvcc -o squareArray squareArray.cu --ptxas-options=-v && ./squareArray`
`nvcc -o addArrays addArrays.cu --ptxas-options=-v && ./addArrays`
