#include <cuda_runtime.h>
#include <iostream>
using namespace std;

// CUDA error checking
#define CUDA_CHECK cuda_check(__FILE__,__LINE__)
void cuda_check(string file, int line) {
    cudaError_t e = cudaGetLastError();
    if (e != cudaSuccess) {
        cout << endl << file << ", line " << line << ": " << cudaGetErrorString(e) << " (" << e << ")" << endl;
        exit(1);
    }
}

__device__ float add(float a, float b) {
    return a + b;
}

__global__ void addArrays(float *a, float *b, float *c, int n) {
    int x = threadIdx.x + blockDim.x * blockIdx.x;
    if (x < n) { c[x] = add(a[x], b[x]); }
}

int main(int argc, char **argv) {
    // Alloc and init input arrays on host (CPU)
    int n = 20;
    float *a = new float[n];
    float *b = new float[n];
    float *c = new float[n];
    for(int i=0; i<n; i++) {
        a[i] = i;
        b[i] = (i%5)+1;
        c[i] = 0;
    }

    // CPU computation
    for(int i=0; i<n; i++) c[i] = a[i] + b[i];

    // Print result
    cout << "CPU:"<<endl;
    for(int i=0; i<n; i++) cout << i << ": " << a[i] << " + " << b[i] << " = " << c[i] << endl;
    cout << endl;
    
    // Init c
    for(int i=0; i<n; i++) c[i] = 0;
    
    // GPU computation
    size_t size = n * sizeof(float);
    
    float *d_a = NULL;
    cudaMalloc(&d_a, size);
    CUDA_CHECK;
    cudaMemcpy(d_a, a, size, cudaMemcpyHostToDevice);
    CUDA_CHECK;
    float *d_b = NULL;
    cudaMalloc(&d_b, size);
    CUDA_CHECK;
    cudaMemcpy(d_b, b, size, cudaMemcpyHostToDevice);
    CUDA_CHECK;
    
    float *d_c = NULL;
    cudaMalloc(&d_c, size);
    CUDA_CHECK;

    dim3 blockDim(32, 1, 1);
    dim3 gridDim((n + blockDim.x - 1) / blockDim.x, 1, 1);
    addArrays<<<gridDim, blockDim>>>(d_a, d_b, d_c, n);
    CUDA_CHECK;

    cudaMemcpy(c, d_c, size, cudaMemcpyDeviceToHost);
    CUDA_CHECK;
    cudaFree(d_c);
    CUDA_CHECK;
    cudaFree(d_b);
    CUDA_CHECK;
    cudaFree(d_a);
    CUDA_CHECK;

    // Print result
    cout << "GPU:"<<endl;
    for(int i=0; i<n; i++) cout << i << ": " << a[i] << " + " << b[i] << " = " << c[i] << endl;
    cout << endl;

    // Free CPU arrays
    delete[] a;
    delete[] b;
    delete[] c;
}

