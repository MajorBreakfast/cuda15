#include <cuda_runtime.h>
#include <iostream>
using namespace std;

// CUDA error checking
#define CUDA_CHECK cuda_check(__FILE__,__LINE__)
void cuda_check(string file, int line) {
    cudaError_t e = cudaGetLastError();
    if (e != cudaSuccess) {
        cout << endl << file << ", line " << line << ": " << cudaGetErrorString(e) << " (" << e << ")" << endl;
        exit(1);
    }
}

__device__ float square(float a) {
    return a * a;
}

__global__ void squareArray(float *a, int n) {
    int x = threadIdx.x + blockDim.x * blockIdx.x;
    if (x < n) { a[x] = square(a[x]); }
}

int main(int argc,char **argv) {
    // Alloc and init input arrays on host (CPU)
    int n = 10;
    float *a = new float[n];
    for(int i=0; i<n; i++) a[i] = i;

    // CPU computation
    for(int i=0; i<n; i++) {
        float val = a[i];
        val = val*val;
        a[i] = val;
    }

    // Print result
    cout << "CPU:"<<endl;
    for(int i=0; i<n; i++) cout << i << ": " << a[i] << endl;
    cout << endl;
    
    // GPU computation
    // Reinit data
    for(int i=0; i<n; i++) a[i] = i;
    
    // GPU computation
    size_t size = n * sizeof(float);
    
    float *d_a = NULL;
    cudaMalloc(&d_a, size);
    CUDA_CHECK;
    cudaMemcpy(d_a, a, size, cudaMemcpyHostToDevice);
    CUDA_CHECK;

    dim3 blockDim(32, 1, 1);
    dim3 gridDim((n + blockDim.x - 1) / blockDim.x, 1, 1);
    squareArray<<<gridDim, blockDim>>>(d_a, n);
    CUDA_CHECK;

    cudaMemcpy(a, d_a, size, cudaMemcpyDeviceToHost);
    CUDA_CHECK;
    cudaFree(d_a);
    CUDA_CHECK;

    // Print result
    cout << "GPU:" << endl;
    for(int i=0; i<n; i++) cout << i << ": " << a[i] << endl;
    cout << endl;

    // Free CPU arrays
    delete[] a;
}

