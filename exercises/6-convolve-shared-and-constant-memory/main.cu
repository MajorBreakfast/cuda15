// ###
// ###
// ### Practical Course: GPU Programming in Computer Vision
// ###
// ###
// ### Technical University Munich, Computer Vision Group
// ### Summer Semester 2015, September 7 - October 6
// ###
// ###
// ### Thomas Moellenhoff, Robert Maier, Caner Hazirbas
// ###
// ###
// ###
// ### THIS FILE IS SUPPOSED TO REMAIN UNCHANGED
// ###
// ###


#include "aux.h"
#include <iostream>
#include <math.h>
using namespace std;

// uncomment to use the camera
#define CAMERA
#define KERNEL_SIZE 1681
__constant__ float d_constKernel[KERNEL_SIZE];



__global__ void mygammaCorr(float *a, float gamma, int n){
	int ind = threadIdx.x + blockDim.x * blockIdx.x;
	if(ind <n) a[ind] = powf(a[ind],gamma);
}
__global__ void mygradient_x(float *inImg, float*gradImg, int w, int h, int n){
	int ind = threadIdx.x + blockDim.x * blockIdx.x;
if(ind <n){
	int tmp = ind % (w*h);
    	int c = (ind-tmp)/(w*h);
        int x = tmp % w;
    	int y = (tmp-x)/w;
	int plusInd = (x+1) + w*y+ w*h*c;
	if((x+1) < w) gradImg[ind] = inImg[plusInd] - inImg[ind];
	else gradImg[ind] = 0;
}
}
__global__ void mygradient_y(float *inImg, float*gradImg, int w, int h, int n){
	int ind = threadIdx.x + blockDim.x * blockIdx.x;
if(ind <n){
	int tmp = ind % (w*h);
    	int c = (ind-tmp)/(w*h);
        int x = tmp % w;
    	int y = (tmp-x)/w;
	int plusInd = x + w*(y+1)+ w*h*c;
	if((y+1) < w) gradImg[ind] = inImg[plusInd] - inImg[ind];
	else gradImg[ind] = 0;
}
}

__global__ void mydivergence(float *v1,float *v2, float *out, int w, int h, int n){
	int ind = threadIdx.x + blockDim.x * blockIdx.x;
if(ind <n){
	int tmp = ind % (w*h);
    	int c = (ind-tmp)/(w*h);
        int x = tmp % w;
    	int y = (tmp-x)/w;
	int minusInd_x = x-1 + w*y+ w*h*c;
	int minusInd_y = x + w*(y-1) + w*h*c;
        float dx = 0;
	float dy = 0;
	if(x>0) dx= v1[ind] - v1[minusInd_x];
	else dx = v1[ind];
	if(y >0) dy = v2[ind] - v2[minusInd_y];
	else dy = v2[ind];
	out[ind] = dx + dy;
}
}

__global__ void mynorm(float *div, float*norm, int w, int h, int n){
	int ind = threadIdx.x + blockDim.x * blockIdx.x;
	if(ind <n){
		int tmp = ind % (w*h);
	 
		norm[tmp] += div[ind]*div[ind];
	}
}
__global__ void mysqrt(float *in, float*out, int n)
{	
	int ind = threadIdx.x + blockDim.x * blockIdx.x;
	if(ind <n){
		out[ind] = sqrt(in[ind]);
	}
}

void gaussian_kernel(float sigma, float *kernel){
	int radius = ceil(3 * sigma);
	float norm = 2*M_PI*sigma*sigma;
	float sum= 0.0;
	int length = 2*radius+1;
	for(int a = -radius; a <= radius; a++){
		for(int b=-radius; b <= radius; b++){
			float tmp = exp( - (float) (a*a+b*b) / (2*sigma*sigma)) / norm;
			kernel[(a+radius)+ length*(b+radius)] = tmp;
			sum+= tmp;
		}	
	}
	for(int i = 0; i < length*length; i++){
	  kernel[i] = kernel[i] / sum;
	  //cout << kernel[i]  << endl;
	}
}
void gaussian_display(float *kernel, float *scaled, int len){
        float max = 0;
	for(int i = 0; i < len; i++){
	  if(kernel[i] > max) max = kernel[i];
	}
	for(int i = 0; i < len; i++){
	  scaled[i] = kernel[i] / max;
	}	
}
void convolute(float *kernel, int radius, float *img, int w, int h, int nc, float *out){
    float sum;
for(int c = 0; c < nc; c++){
    for(int y = 0; y < h; y++){
        for(int x = 0; x < w; x++){
            sum = 0.0;
            for(int k = -radius; k <= radius;k++){
                for(int j = -radius; j <=radius; j++){
    		    int x1 = x - k;
		    int y1 = y - j;
		    if(x1 < 0) x1 = 0;
		    if(y1 <0) y1 = 0;
		    if(x1 >= w) x1 = w-1;
		    if(y1 >= h) y1 = h -1; 
		    sum = sum + kernel[(j+radius)+ (k+radius)*(2*radius+1)]*img[x1 + y1*w + c*w*h];
                }
            }
            out[x+ y*w + c*w*h] = sum;
        }
    }
}
}

inline __device__ __host__ int clamp(int val, int l, int u) {
    return max(l, min(u, val));
}

__global__ void d_convolute(int r, float *img, int w, int h, float *out) {
    int x = blockDim.x * blockIdx.x + threadIdx.x;
    int y = blockDim.y * blockIdx.y + threadIdx.y;

    // (sx, sy, sw, sh) define the rect of the shared memory image in the global image
    int sx = blockDim.x * blockIdx.x - r;
    int sy = blockDim.y * blockIdx.y - r;    
    int sw = blockDim.x + 2*r;
    int sh = blockDim.y + 2*r;

    // Fill shared memory image
    extern __shared__ float sh_img[];
    int t = blockDim.x*blockDim.y;

    for (int i = threadIdx.x + threadIdx.y*blockDim.x ; i < sw*sh; i += t) {
        // (u, v) is the postion within the shared memory image
        int u = i % sw;
        int v = (i - u) / sw;     
        sh_img[i] = img[clamp(sx + u, 0, w-1) + clamp(sy + v, 0, h-1) * w];
    }

    __syncthreads();

    // Convolve
	if(x < w && y < h) {
	    float sum = 0.0;
	    
	    for(int b=-r; b <= r; b++) {
            for(int a=-r; a <= r; a++) {
	            sum += d_constKernel[(a+r) + (b+r)*(2*r+1)] * sh_img[(x-sx-a) + (y-sy-b)*sw];
            }
        }

    	out[x + y*w] = sum;
    }
}

int main(int argc, char **argv)
{
    // Before the GPU can process your kernels, a so called "CUDA context" must be initialized
    // This happens on the very first call to a CUDA function, and takes some time (around half a second)
    // We will do it right here, so that the run time measurements are accurate
    cudaDeviceSynchronize();  CUDA_CHECK;


    // Reading command line parameters:
    // getParam("param", var, argc, argv) looks whether "-param xyz" is specified, and if so stores the value "xyz" in "var"
    // If "-param" is not specified, the value of "var" remains unchanged
    //
    // return value: getParam("param", ...) returns true if "-param" is specified, and false otherwise


#ifdef CAMERA
#else
    // input image
    string image = "";
    bool ret = getParam("i", image, argc, argv);
    if (!ret) cerr << "ERROR: no image specified" << endl;
    if (argc <= 1) { cout << "Usage: " << argv[0] << " -i <image> [-repeats <repeats>] [-gray]" << endl; return 1; }
#endif
    
    // number of computation repetitions to get a better run time measurement
    int repeats = 1;
    getParam("repeats", repeats, argc, argv);
    cout << "repeats: " << repeats << endl;
    
    // load the input image as grayscale if "-gray" is specifed
    bool gray = false;
    getParam("gray", gray, argc, argv);
    cout << "gray: " << gray << endl;

    // ### Define your own parameters here as needed    
    float gamma = 0.0;
    getParam("gamma", gamma, argc, argv);
    cout << "gamma: " << gamma << endl;

    float sigma = 2.0;
    getParam("sigma", sigma, argc, argv);
    cout << "sigma: " << sigma << endl;

   //Gausian Kernel:
    int radius =  ceil(sigma*3);
    cout << "radius :" << radius << endl;
    int diameter= 2*radius+1;
    cv::Mat gauss(diameter,diameter,CV_32FC1);
    
    int kernel_size =diameter*diameter;
    float *kernel = (float* ) malloc(kernel_size *sizeof(float));
    float *k_prime = (float* ) malloc(kernel_size *sizeof(float));
    gaussian_kernel(sigma,kernel);
    gaussian_display(kernel,k_prime, kernel_size);
    cudaMemcpyToSymbol(d_constKernel, kernel, kernel_size * sizeof(float));CUDA_CHECK;
    
    // Init camera / Load input image
#ifdef CAMERA

    // Init camera
  	cv::VideoCapture camera(0);
  	if(!camera.isOpened()) { cerr << "ERROR: Could not open camera" << endl; return 1; }
    int camW = 640;
    int camH = 480;
  	camera.set(CV_CAP_PROP_FRAME_WIDTH,camW);
  	camera.set(CV_CAP_PROP_FRAME_HEIGHT,camH);
    // read in first frame to get the dimensions
    cv::Mat mIn;
    camera >> mIn;
    
#else
    
    // Load the input image using opencv (load as grayscale if "gray==true", otherwise as is (may be color or grayscale))
    cv::Mat mIn = cv::imread(image.c_str(), (gray? CV_LOAD_IMAGE_GRAYSCALE : -1));
    // check
    if (mIn.data == NULL) { cerr << "ERROR: Could not load image " << image << endl; return 1; }
    
#endif

    // convert to float representation (opencv loads image values as single bytes by default)
    mIn.convertTo(mIn,CV_32F);
    // convert range of each channel to [0,1] (opencv default is [0,255])
    mIn /= 255.f;
    // get image dimensions
    int w = mIn.cols;         // width
    int h = mIn.rows;         // height
    int nc = mIn.channels();  // number of channels
    cout << "image: " << w << " x " << h << endl;




    // Set the output image format
    // ###
    // ###
    // ### TODO: Change the output image format as needed
    
    // ###
    // ###
    cv::Mat mOut(h,w,mIn.type());  // mOut will have the same number of channels as the input image, nc layers
    //cv::Mat mOut(h,w,CV_32FC3);    // mOut will be a color image, 3 layers
    //cv::Mat mOut(h,w,CV_32FC1);    // mOut will be a grayscale image, 1 layer
    cv::Mat mGaussian(diameter,diameter,CV_32FC1);
    // ### Define your own output images here as needed

	


    // Allocate arrays
    // input/output image width: w
    // input/output image height: h
    // input image number of channels: nc
    // output image number of channels: mOut.channels(), as defined above (nc, 3, or 1)

    // allocate raw input image array
    float *imgIn  = new float[(size_t)w*h*nc];

    // allocate raw output array (the computation result will be stored in this array, then later converted to mOut for displaying)
    float *imgOut = new float[(size_t)w*h*mOut.channels()];
    //allocate device memory
    float *d_imgIn,*d_imgOut; //*d_kernel,
    cudaMalloc(&d_imgIn, w*h*nc * sizeof(float));CUDA_CHECK;
    //cudaMalloc(&d_kernel, kernel_size * sizeof(float));CUDA_CHECK;
    cudaMalloc(&d_imgOut, w*h*nc * sizeof(float));CUDA_CHECK;

    
	
    // For camera mode: Make a loop to read in camera frames
#ifdef CAMERA
    // Read a camera image frame every 30 milliseconds:
    // cv::waitKey(30) waits 30 milliseconds for a keyboard input,
    // returns a value <0 if no key is pressed during this time, returns immediately with a value >=0 if a key is pressed
    while (cv::waitKey(30) < 0)
    {
    // Get camera image
    camera >> mIn;
    // convert to float representation (opencv loads image values as single bytes by default)
    mIn.convertTo(mIn,CV_32F);
    // convert range of each channel to [0,1] (opencv default is [0,255])
    mIn /= 255.f;
#endif

    // Init raw input image array
    // opencv images are interleaved: rgb rgb rgb...  (actually bgr bgr bgr...)
    // But for CUDA it's better to work with layered images: rrr... ggg... bbb...
    // So we will convert as necessary, using interleaved "cv::Mat" for loading/saving/displaying, and layered "float*" for CUDA computations
    convert_mat_to_layered (imgIn, mIn);


    
    Timer timer; 

    //timer.start();
    // ###
    // ###
    // ### TODO: Main computation
    // ###
    // ###


    //cpu convolution:
   //convolute(kernel, radius, imgIn, w,h,nc,imgOut);
  
    
    //timer.end();  
float t;
    //float t = timer.get();  // elapsed time in seconds
    //cout << "CPU Elapsed time: " << t*1000 << " ms" << endl;
    
    //gpu convolution:
    timer.start();
    
    //copy host memory to device
    cudaMemcpy(d_imgIn, imgIn, w*h*nc * sizeof(float), cudaMemcpyHostToDevice);CUDA_CHECK;
    
    //lauch kernel
    dim3 blockDim = dim3(32, 8, 1); 
    dim3 gridDim = dim3((w+ blockDim.x-1)/blockDim.x, (h + blockDim.y -1) / blockDim.y, 1);
    int sharedMemBytes = (blockDim.x+2*radius) * (blockDim.y+2*radius) * sizeof(d_imgIn[0]);
    for (int c=0; c<nc; c++) {
        d_convolute<<<gridDim, blockDim, sharedMemBytes>>>(radius, d_imgIn+w*h*c, w, h, d_imgOut+w*h*c);
    }
    CUDA_CHECK;
    

    //copy result back to host memory
    cudaMemcpy(imgOut, d_imgOut, w*h*nc*sizeof(float), cudaMemcpyDeviceToHost);CUDA_CHECK;


    timer.end();
    t = timer.get();  // elapsed time in seconds
    cout << "GPU Elapsed time: " << t*1000 << " ms" << endl;

    // show input image
    showImage("Input", mIn, 100, 100);  // show at position (x_from_left=100,y_from_above=100)
    convert_layered_to_mat(mGaussian, k_prime);
    //showImage("Gaussian", mGaussian, 100, 100);

    // show output image: first convert to interleaved opencv format from the layered raw array
    convert_layered_to_mat(mOut, imgOut);
    showImage("Output", mOut, 100+w+40, 100);
    // ### Display your own output images here as needed




#ifdef CAMERA
    // end of camera loop
    }
#else
    // wait for key inputs
    cv::waitKey(0);
#endif

    //free device memory:
    cudaFree(d_imgIn);CUDA_CHECK;
    //cudaFree(d_kernel);CUDA_CHECK;
    cudaFree(d_imgOut);CUDA_CHECK;


    // save input and result
    cv::imwrite("image_input.png",mIn*255.f);  // "imwrite" assumes channel range [0,255]
    cv::imwrite("image_result.png",mOut*255.f);

    // free allocated arrays
    delete[] imgIn;
    delete[] imgOut;
    //free(kernel);
    // close all opencv windows
    cvDestroyAllWindows();
    return 0;
}



