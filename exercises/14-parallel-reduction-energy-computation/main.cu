#include "aux.h"
#include <iostream>
#include "cublas_v2.h"

using namespace std;

__global__ void d_sum(float *array, int n, int stride) {
	extern __shared__ float sdata[];
	int offset = blockDim.x * blockIdx.x * 2;
	int i = threadIdx.x;

    int pos = (offset+i)*stride;
	float a = pos < n ? array[pos] : 0;
	
	for (int threadCount = blockDim.x; threadCount > 0; threadCount >>= 1) {
        // Load values and compute sum
		if (i < threadCount) { // Active?
			float b;
			if (threadCount == blockDim.x) { // First iteration?
                int pos = (offset+threadCount+i)*stride;
                b = pos < n ? array[pos] : 0;
			} else {
				b = sdata[i];
			}
			a += b;
        }

        __syncthreads();

        // Write values to shared memory for next iteration
        if (i < threadCount) { // Active?
			int pos = i - (threadCount >> 1);
			if (pos >= 0) { sdata[pos] = a; }
		}

	    __syncthreads();
	}

	if (i == 0) { array[offset*stride] = a; }
}

int main(int argc, char **argv) {
    int arraySize = 2174836;

	float *array = new float[arraySize];
	float sum = 0; // For comparison
	
	for (int i = 0; i < arraySize; i++) {
        float val = 256 + (i % 256) - 127; // Some formula
		array[i] = val;
		sum += val;
	}

	cout << "Array sum computed by CPU: " << sum << endl;

	// Allocate CUDA buffers
	float *d_array;
	cudaMalloc(&d_array, arraySize * sizeof(float));
	cudaMemcpy(d_array, array, arraySize * sizeof(float), cudaMemcpyHostToDevice);
	CUDA_CHECK;

    Timer timer; float t;

    //cublas
   
    cublasStatus_t ret;
	cublasHandle_t handle;
	cublasCreate(&handle);
    float cb_result = 0.0f;

    timer.start();

	ret = cublasSasum(handle, arraySize, d_array, 1, &cb_result);

    timer.end();  t = timer.get();  // elapsed time in seconds
    
    cout << "status: " << ret << endl;

    cout << "cublas Sum : " << cb_result <<endl;
    cout << "cublas time: " << t*1000 << " ms" << endl;


    cudaMemcpy(d_array, array, arraySize * sizeof(float), cudaMemcpyHostToDevice);
    cudaDeviceSynchronize();
    
    timer.start();
	// Launch kernel
    {    

        dim3 blockDim = dim3(1024, 1, 1);

        int blockCount = (arraySize + blockDim.x*2 - 1) / (blockDim.x*2);
        int stride = 1;
        while(true) {
            dim3 gridDim(blockCount, 1, 1);

	        int sharedMemBytes = blockDim.x / 2 * sizeof(float);
            d_sum<<<gridDim, blockDim, sharedMemBytes>>>(d_array, arraySize, stride);
	        CUDA_CHECK;
            
            if (blockCount == 1) break;
            
            stride *= blockDim.x*2;
            blockCount = (blockCount + blockDim.x*2 - 1) / (blockDim.x*2);
       }

    }
    
	cudaDeviceSynchronize();
	float result = 0;
	cudaMemcpy(&result, d_array, sizeof(float), cudaMemcpyDeviceToHost);
	CUDA_CHECK;
    timer.end();  t = timer.get();  // elapsed time in seconds
	cout << "Array sum computed by GPU: " << result << endl;
    cout << "manual implementation time: " << t*1000 << " ms" << endl;


	// Free CUDA buffers
    cublasDestroy(handle);
    cudaFree(d_array);
	CUDA_CHECK;

	delete[] array;

    return 0;
}

