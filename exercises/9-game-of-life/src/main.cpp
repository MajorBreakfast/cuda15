#include <time.h>
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <cuda_runtime.h>

#include <gameOfLife.h>
#include <BoardVisualization.hpp>

#include "aux.h"

#define BOARD_SIZE_X 340
#define BOARD_SIZE_Y 175
#define CIRCLE_RADIUS 2


void initBoardAtRandom(cv::Mat& board) {
    srand(time(NULL));
    for (int i = 0; i < board.rows; i++) {
        for (int j = 0; j < board.cols; j++) {
            board.at<unsigned char>(i, j) = static_cast<unsigned char>(rand() % 2);
        }
    }
}


int main(int argc, const char *argv[])
{
    unsigned char* d_src;
    unsigned char* d_dst;
    unsigned char* d_tmp;

    cv::Mat board = cv::Mat::zeros(BOARD_SIZE_X, BOARD_SIZE_Y, CV_8UC1);
    BoardVisualization viewer(BOARD_SIZE_X, BOARD_SIZE_Y, CIRCLE_RADIUS);

    // Initialize the board randomly
    initBoardAtRandom(board);

    int board_size = BOARD_SIZE_X * BOARD_SIZE_Y;

    cudaMalloc(&d_src, board_size);
    CUDA_CHECK;
    cudaMemcpy(d_src, board.data, board_size, cudaMemcpyHostToDevice);
    CUDA_CHECK;
    cudaMalloc(&d_dst, board_size);
    CUDA_CHECK;


    int key;
    while (key = cv::waitKey(1)) {

        runGameOfLifeIteration(d_src, d_dst, BOARD_SIZE_Y, BOARD_SIZE_X);

        cudaMemcpy(board.data, d_dst, board_size, cudaMemcpyDeviceToHost);
        CUDA_CHECK;
               
        d_tmp = d_src; d_src = d_dst; d_dst = d_tmp; // Swap


        /** This is just for display. You should not touch this.  **/
        viewer.displayBoard(board);
        if (key != -1) {
            if(key == 1048608) { // space key
                initBoardAtRandom(board);
                cudaMemcpy(d_src, board.data, board_size, cudaMemcpyHostToDevice);
                CUDA_CHECK;
            } else {
                break;
            }
        }
    }

    cudaFree(d_src);
    cudaFree(d_dst);

    return 0;
}
