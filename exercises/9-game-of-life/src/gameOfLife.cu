#include <cuda_runtime.h>
#include <gameOfLife.h>
#include "aux.h"

#define BLOCK_SIZE_X 16
#define BLOCK_SIZE_Y 16


__device__ int alive(int x, int y, int h, int w, unsigned char* d_src) {
    if(x<0) { return 0; }
    if(x>=w) { return 0; }
    if(y<0) { return 0; }
    if(y>=h) { return 0; }
    return d_src[x + y*w];
}

__global__ void gameOfLifeKernel(unsigned char* d_src, unsigned char* d_dst, const size_t width, const size_t height) {
    int x = threadIdx.x + blockDim.x * blockIdx.x;
    int y = threadIdx.y + blockDim.y * blockIdx.y;

    if(x<width && y<height) {

        int numberOfNeighbours =  alive(x-1,  y, height, width, d_src) 
                                + alive(x+1,  y, height, width, d_src) 
                                + alive(x,  y-1, height, width, d_src) 
                                + alive(x,  y+1, height, width, d_src) 
                                + alive(x+1,y+1, height, width, d_src) 
                                + alive(x-1,y-1, height, width, d_src) 
                                + alive(x+1,y-1, height, width, d_src) 
                                + alive(x-1,y+1, height, width, d_src);  
        
        if(d_src[x + y*width] == 1) {
            if(numberOfNeighbours < 2) {
                d_dst[x + y*width] = 0;
            } 
            if(numberOfNeighbours >= 2 && numberOfNeighbours <= 3) {
                d_dst[x + y*width] = 1;
            }
            if(numberOfNeighbours > 3) {
                d_dst[x + y*width] = 0;
            }
        } else { //failsave
            d_dst[x + y*width] = 0;
        }
        if(d_src[x + y*width] == 0 && numberOfNeighbours == 3) {
            d_dst[x + y*width] = 1;
        }

    }
}

void runGameOfLifeIteration(unsigned char* d_src, unsigned char* d_dst, const size_t width, const size_t height) {

    dim3 blockDim(BLOCK_SIZE_X, BLOCK_SIZE_Y, 1);
    dim3 gridDim((width + blockDim.x - 1) / blockDim.x, (height + blockDim.y - 1) / blockDim.y, 1);

    gameOfLifeKernel<<<gridDim,blockDim>>>(d_src, d_dst, width, height);
}
