#include "aux.h"
#include <iostream>
using namespace std;

// uncomment to use the camera
#define CAMERA

inline dim3 calcGridDim2d(dim3 blockDim, int w, int h) {
    return dim3(
        (w + blockDim.x - 1) / blockDim.x, 
        (h + blockDim.y - 1) / blockDim.y,
        1
    );
}

inline __device__ int lin(int x, int y, int w) {
  return x + w*y;
}

inline __host__ __device__ float g_diff(float s, int type, float epsilon){
    float g;
    if(type == 0) {
        g = 1;
    } else if (type == 1) {
        g = epsilon > s ? 1.0/epsilon : 1.0/s;
    } else {
        g = exp(-s*s/epsilon)/epsilon;
    }
    return g;
}

inline __device__ float s_diff(float *v1, float *v2, int x, int y, int w) {
    if (x < w) {
        int idx = lin(x, y, w);
        float s = v1[idx] * v1[idx] + v2[idx] * v2[idx];

        return sqrtf(s);
    } else {
        return 0;
    }
}

__global__ void d_diffusivity(float *img, float *v1, float *v2, int w, int h, float epsilon, float tau, int type) {
    
    int x = threadIdx.x + blockDim.x * blockIdx.x;
    int y = threadIdx.y + blockDim.y * blockIdx.y;

    if (x < w && y < h) {
        float g =      g_diff(s_diff(v1, v2, x  , y, w), type, epsilon);
        float g_left = g_diff(s_diff(v1, v2, max(0, x-1), y, w), type, epsilon);
        float g_top =  g_diff(s_diff(v1, v2, x, max(0, y-1), w), type, epsilon);
        
        int idx = lin(x, y, w);

        float div = x > 0 ? g * v1[idx] - g_left * v1[lin(x-1, y, w)] : g * v1[idx] +
                    y > 0 ? g * v2[idx] - g_top  * v2[lin(x, y-1, w)] : g * v2[idx];
        img[idx] += tau * div;
    }
}

__global__ void d_gradient(float *img, float *v1, float *v2, int w, int h) {
    int x = threadIdx.x + blockDim.x * blockIdx.x;
    int y = threadIdx.y + blockDim.y * blockIdx.y;
    if (x < w && y < h) {
        int idx = lin(x, y, w);
        v1[idx] = x + 1 < w ? img[lin(x+1, y, w)] - img[idx] : 0;
        v2[idx] = y + 1 < h ? img[lin(x, y+1, w)] - img[idx] : 0;
    }
}



int main(int argc, char **argv)
{
    // Before the GPU can process your kernels, a so called "CUDA context" must be initialized
    // This happens on the very first call to a CUDA function, and takes some time (around half a second)
    // We will do it right here, so that the run time measurements are accurate
    cudaDeviceSynchronize();  CUDA_CHECK;




    // Reading command line parameters:
    // getParam("param", var, argc, argv) looks whether "-param xyz" is specified, and if so stores the value "xyz" in "var"
    // If "-param" is not specified, the value of "var" remains unchanged
    //
    // return value: getParam("param", ...) returns true if "-param" is specified, and false otherwise

#ifdef CAMERA
#else
    // input image
    string image = "";
    bool ret = getParam("i", image, argc, argv);
    if (!ret) cerr << "ERROR: no image specified" << endl;
    if (argc <= 1) { cout << "Usage: " << argv[0] << " -i <image> [-repeats <repeats>] [-gray]" << endl; return 1; }
#endif
    
    // number of computation repetitions to get a better run time measurement
    int N = 1;
    getParam("N", N, argc, argv);
    cout << "N: " << N << endl;

    float epsilon = 0.05;
    getParam("epsilon", epsilon, argc, argv);
    cout << "epsilon: " << epsilon << endl;

    float tau = 0.15;
    getParam("tau", tau, argc, argv);
    cout << "tau: " << tau << endl;

    int diff_type = 0;
    getParam("diff",diff_type, argc, argv);
    cout << "diff: " << diff_type << endl;


    // load the input image as grayscale if "-gray" is specifed
    bool gray = false;
    getParam("gray", gray, argc, argv);
    cout << "gray: " << gray << endl;

    // ### Define your own parameters here as needed    


    // Init camera / Load input image
#ifdef CAMERA

    // Init camera
  	cv::VideoCapture camera(0);
  	if(!camera.isOpened()) { cerr << "ERROR: Could not open camera" << endl; return 1; }
    int camW = 640;
    int camH = 480;
  	camera.set(CV_CAP_PROP_FRAME_WIDTH,camW);
  	camera.set(CV_CAP_PROP_FRAME_HEIGHT,camH);
    // read in first frame to get the dimensions
    cv::Mat mIn;
    camera >> mIn;
    
#else
    
    // Load the input image using opencv (load as grayscale if "gray==true", otherwise as is (may be color or grayscale))
    cv::Mat mIn = cv::imread(image.c_str(), (gray? CV_LOAD_IMAGE_GRAYSCALE : -1));
    // check
    if (mIn.data == NULL) { cerr << "ERROR: Could not load image " << image << endl; return 1; }
    
#endif

    // convert to float representation (opencv loads image values as single bytes by default)
    mIn.convertTo(mIn,CV_32F);
    // convert range of each channel to [0,1] (opencv default is [0,255])
    mIn /= 255.f;
    // get image dimensions
    int w = mIn.cols;         // width
    int h = mIn.rows;         // height
    int nc = mIn.channels();  // number of channels
    cout << "image: " << w << " x " << h << endl;




    // Set the output image format
    // ###
    // ###
    // ### TODO: Change the output image format as needed
    // ###
    // ###
    cv::Mat mOut(h,w,mIn.type());  // mOut will have the same number of channels as the input image, nc layers
    //cv::Mat mOut(h,w,CV_32FC3);    // mOut will be a color image, 3 layers
    //cv::Mat mOut(h,w,CV_32FC1);    // mOut will be a grayscale image, 1 layer
    // ### Define your own output images here as needed




    // Allocate arrays
    // input/output image width: w
    // input/output image height: h
    // input image number of channels: nc
    // output image number of channels: mOut.channels(), as defined above (nc, 3, or 1)

    // allocate raw input image array
    float *imgIn  = new float[(size_t)w*h*nc];

    // allocate raw output array (the computation result will be stored in this array, then later converted to mOut for displaying)
    float *imgOut = new float[(size_t)w*h*mOut.channels()];


    // Allocate CUDA buffers
    size_t size = sizeof(float)*w*h*nc;

    float *d_imgIn;
    cudaMalloc(&d_imgIn, size);
    CUDA_CHECK;

    float *d_v1;
    cudaMalloc(&d_v1, size/3);
    CUDA_CHECK;

    float *d_v2;
    cudaMalloc(&d_v2, size/3);
    CUDA_CHECK;

    float *d_imgOut;
    cudaMalloc(&d_imgOut, size);
    CUDA_CHECK;


    // For camera mode: Make a loop to read in camera frames
#ifdef CAMERA
    // Read a camera image frame every 30 milliseconds:
    // cv::waitKey(30) waits 30 milliseconds for a keyboard input,
    // returns a value <0 if no key is pressed during this time, returns immediately with a value >=0 if a key is pressed
    while (cv::waitKey(30) < 0)
    {
    // Get camera image
    camera >> mIn;
    // convert to float representation (opencv loads image values as single bytes by default)
    mIn.convertTo(mIn,CV_32F);
    // convert range of each channel to [0,1] (opencv default is [0,255])
    mIn /= 255.f;
#endif

    // Init raw input image array
    // opencv images are interleaved: rgb rgb rgb...  (actually bgr bgr bgr...)
    // But for CUDA it's better to work with layered images: rrr... ggg... bbb...
    // So we will convert as necessary, using interleaved "cv::Mat" for loading/saving/displaying, and layered "float*" for CUDA computations
    convert_mat_to_layered (imgIn, mIn);


    // GPU computation
    Timer timer; timer.start();
    
    cudaMemcpy(d_imgIn, imgIn, size, cudaMemcpyHostToDevice);
    CUDA_CHECK;

    dim3 blockDim(16, 16, 1);
    dim3 gridDim = calcGridDim2d(blockDim, w, h);

    
    for (int c=0; c<nc; c++) {
        for (int i=0; i<N; i++) {
            d_gradient<<<gridDim, blockDim>>>(d_imgIn + w*h*c, d_v1, d_v2, w, h);
            CUDA_CHECK;
            d_diffusivity<<<gridDim, blockDim>>>(d_imgIn + w*h*c, d_v1, d_v2, w, h, epsilon, tau, diff_type);
            CUDA_CHECK;
        }
    }
    
    

    cudaMemcpy(imgOut, d_imgIn, size, cudaMemcpyDeviceToHost);
    CUDA_CHECK;

    timer.end();  float t = timer.get();  // elapsed time in seconds
    cout << "time: " << t*1000 << " ms" << endl;


    // show input image
    showImage("Input", mIn, 100, 100);  // show at position (x_from_left=100,y_from_above=100)

    // show output image: first convert to interleaved opencv format from the layered raw array
    convert_layered_to_mat(mOut, imgOut);
    showImage("Output", mOut, 100+w+40, 100);

    // ### Display your own output images here as needed

#ifdef CAMERA
    // end of camera loop
    }
#else
    // wait for key inputs
    cv::waitKey(0);
#endif

    
    // CUDA cleanup
    cudaFree(d_imgOut);
    CUDA_CHECK;
    cudaFree(d_v2);
    CUDA_CHECK;
    cudaFree(d_v1);
    CUDA_CHECK;    
    cudaFree(d_imgIn);
    CUDA_CHECK;


    // save input and result
    cv::imwrite("image_input.png",mIn*255.f);  // "imwrite" assumes channel range [0,255]
    cv::imwrite("image_result.png",mOut*255.f);

    // free allocated arrays
    delete[] imgIn;
    delete[] imgOut;

    // close all opencv windows
    cvDestroyAllWindows();
    return 0;
}



