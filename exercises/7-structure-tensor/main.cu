#include "aux.h"
#include <iostream>
#include <math.h>
using namespace std;

// uncomment to use the camera
#define CAMERA

inline __device__ __host__ int clamp(int val, int l, int u) {
    return max(l, min(u, val));
}

__global__ void d_gaussianBlurX(float *dest, float *src, int w, int h, int r) {
    int x = blockDim.x * blockIdx.x + threadIdx.x;
    int y = blockDim.y * blockIdx.y + threadIdx.y;

    // (sx, sy, sw, sh) define the rect of the shared memory image in the global image
    int sx = blockDim.x*blockIdx.x - r;
    int sy = blockDim.y*blockIdx.y;
    int sw = blockDim.x + 2*r;
    int sh = blockDim.y;

    // Fill shared memory image
    extern __shared__ float sh_img[];
    int t = blockDim.x*blockDim.y;

    for (int i = threadIdx.x + threadIdx.y*blockDim.x ; i < sw*sh; i += t) {
        // (u, v) is the postion within the shared memory image
        int u = i % sw;
        int v = (i - u) / sw;
        sh_img[i] = src[clamp(sx + u, 0, w-1) + clamp(sy + v, 0, h-1) * w];
    }

    __syncthreads();

    // Convolve
	if(x < w && y < h) {
        float sum = 0.0;
        float sig = (float)r / 3;
        
        for (int a = -r; a <= r; a++) {
            float k = exp(-(float)a*a / (2*sig*sig)) / (sqrt(2*3.141592654f)*sig);
            sum += k * sh_img[(x-sx-a) + (y-sy)*sw];
        }

    	dest[x + y*w] = sum;
    }
}

__global__ void d_flip(float *dest, float *src, int w, int h) {
    int x = blockDim.x * blockIdx.x + threadIdx.x;
    int y = blockDim.y * blockIdx.y + threadIdx.y;
    if(x < w && y < h) { dest[y + x*h] = src[x + y*w]; }
}

void gaussianBlurX(float *d_dst, float *d_src, int w, int h, int r) {
    dim3 blockDim = dim3(128, 1, 1);
    dim3 gridDim = dim3((w+blockDim.x-1)/blockDim.x, (h+blockDim.y-1)/blockDim.y, 1);
    int sharedMemBytes = (blockDim.x+2*r)*blockDim.y*sizeof(d_src[0]);
    d_gaussianBlurX<<<gridDim, blockDim, sharedMemBytes>>>(d_dst, d_src, w, h, r);
}

void flip(float *d_dst, float *d_src, int w, int h) {
    dim3 blockDim = dim3(32, 8, 1);
    dim3 gridDim = dim3((w+blockDim.x-1)/blockDim.x, (h+blockDim.y-1)/blockDim.y, 1);
    d_flip<<<gridDim, blockDim>>>(d_dst, d_src, w, h);
}

int main(int argc, char **argv) {
    cudaDeviceSynchronize();
    CUDA_CHECK;

#ifdef CAMERA
#else
    // Image parameter
    string image = "";
    bool ret = getParam("i", image, argc, argv);
    if (!ret) cerr << "ERROR: no image specified" << endl;
    if (argc <= 1) { cout << "Usage: " << argv[0] << " -i <image> [-repeats <repeats>] [-gray]" << endl; return 1; }
#endif
    
    // Repetitions parameter
    int repeats = 1;
    getParam("repeats", repeats, argc, argv);
    cout << "repeats: " << repeats << endl;
    
    // Grayscale parameter
    bool gray = false;
    getParam("gray", gray, argc, argv);
    cout << "gray: " << gray << endl;

#ifdef CAMERA
    // Init camera
  	cv::VideoCapture camera(0);
  	if(!camera.isOpened()) { cerr << "ERROR: Could not open camera" << endl; return 1; }
    int camW = 640, camH = 480;
  	camera.set(CV_CAP_PROP_FRAME_WIDTH, camW);
  	camera.set(CV_CAP_PROP_FRAME_HEIGHT, camH);
    // read in first frame to get the dimensions
    cv::Mat mIn;
    camera >> mIn; 
#else
    // Load the input image
    cv::Mat mIn = cv::imread(image.c_str(), (gray? CV_LOAD_IMAGE_GRAYSCALE : -1));
    if (mIn.data == NULL) { cerr << "ERROR: Could not load image " << image << endl; return 1; }
#endif

    // Convert input to float
    mIn.convertTo(mIn, CV_32F);
    mIn /= 255.f;

    // Get dimensions
    int w = mIn.cols;         // width
    int h = mIn.rows;         // height
    int nc = mIn.channels();  // number of channels
    cout << "image: " << w << " x " << h << endl;
    
    // Create output image
    cv::Mat mOut(h, w, mIn.type());

	// Allocate arrays
    float *imgIn  = new float[(size_t)w*h*nc];
    float *imgOut = new float[(size_t)w*h*mOut.channels()];

    // Allocate device memory
    float *d_imgIn, *d_imgOut;
    cudaMalloc(&d_imgIn, w*h*nc * sizeof(float));CUDA_CHECK;
    cudaMalloc(&d_imgOut, w*h*nc * sizeof(float));CUDA_CHECK;

#ifdef CAMERA
    // Read a camera image frame every 30 milliseconds:
    while (cv::waitKey(30) < 0) {
    camera >> mIn;
    mIn.convertTo(mIn,CV_32F);
    mIn /= 255.f;
#endif

    // Init raw input image array
    convert_mat_to_layered (imgIn, mIn);

    // Start timer
    Timer timer; 
    timer.start();
    
    // Copy host memory to device
    cudaMemcpy(d_imgIn, imgIn, w*h*nc * sizeof(float), cudaMemcpyHostToDevice);
    CUDA_CHECK;

    // Launch kernel
    {
        int r = 100;

        for (int c=0; c < nc; c++) {
            gaussianBlurX(d_imgOut+w*h*c, d_imgIn+w*h*c, w, h, r);
            flip(d_imgIn+w*h*c, d_imgOut+w*h*c, w, h);
            gaussianBlurX(d_imgOut+w*h*c, d_imgIn+w*h*c, h, w, r);
            flip(d_imgIn+w*h*c, d_imgOut+w*h*c, h, w);
        }
        CUDA_CHECK;

        float *d_tmp = d_imgIn; d_imgIn = d_imgOut; d_imgOut = d_tmp; // Swap pointers
    }

    // Copy result back to host memory
    cudaMemcpy(imgOut, d_imgOut, w*h*nc*sizeof(float), cudaMemcpyDeviceToHost);
    CUDA_CHECK;

    // Stop and read timer
    timer.end();
    float t = timer.get();  // elapsed time in seconds
    cout << "GPU Elapsed time: " << t*1000 << " ms" << endl;

    // Fill output image
    convert_layered_to_mat(mOut, imgOut);

    // Show images
    showImage("Input", mIn, 100, 100);
    showImage("Output", mOut, 100+w+40, 100);

#ifdef CAMERA
    } // end of camera loop
#else
    cv::waitKey(0); // wait for key inputs
#endif

    // Free device memory:
    cudaFree(d_imgIn); CUDA_CHECK;
    cudaFree(d_imgOut); CUDA_CHECK;

    // Save input and result
    cv::imwrite("image_input.png", mIn*255.f);  // "imwrite" assumes channel range [0,255]
    cv::imwrite("image_result.png", mOut*255.f);

    // Free allocated arrays
    delete[] imgIn;
    delete[] imgOut;

    // Close all opencv windows
    cvDestroyAllWindows();

    return 0;
}

