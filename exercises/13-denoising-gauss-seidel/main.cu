#include "aux.h"
#include <iostream>
using namespace std;

// uncomment to use the camera
//#define CAMERA

inline dim3 calcGridDim2d(dim3 blockDim, int w, int h) {
    return dim3(
        (w + blockDim.x - 1) / blockDim.x, 
        (h + blockDim.y - 1) / blockDim.y,
        1
    );
}

inline __device__ float huber(float s, float eps) {
    return s < eps ? s*s / (2.0*eps) : s - (eps / 2.0);
}

inline __device__ float g_hat(float s, float eps) {
    return 1.0 / max(eps, s);
}

inline __device__ int lin(int x, int y, int w) {
  return x + w*y;
}

__global__ void d_g(float *gImg, float *srcImg, int w, int h, float eps, bool red) {
    int y = threadIdx.y + blockDim.y * blockIdx.y;    
    int x = (threadIdx.x + blockDim.x * blockIdx.x) * 2 + ((y + red) % 2);

    if (x < w && y < h) {
        float u   = srcImg[lin(x, y,             w)];
        float u_r = srcImg[lin(min(x+1, w-1), y, w)];
        float u_d = srcImg[lin(x, min(y+1, h-1), w)];
        float v1 = u_r - u;
        float v2 = u_d - u;
        float v = sqrtf(v1*v1 + v2*v2);
        gImg[x + y*w] = g_hat(v, eps);
    }
}

__global__ void d_gaussSeidel(float *uDstImg, float *uSrcImg, float *fImg, float *gImg, int w, int h, float lambda, bool red) {
    int y = threadIdx.y + blockDim.y * blockIdx.y;    
    int x = (threadIdx.x + blockDim.x * blockIdx.x) * 2 + ((y + red) % 2);
    
    if (x < w && y < h) {
        float f = fImg[lin(x,y,w)];

        float g_r = gImg   [lin(min(w-1,x+1), y, w)];
        float u_r = uSrcImg[lin(min(w-1,x+1), y, w)];

        float g_l = gImg   [lin(max(0,x-1),   y, w)];
        float u_l = uSrcImg[lin(max(0,x-1),   y, w)];
        
        float g_u = gImg   [lin(x, max(0,y-1),   w)];
        float u_u = uSrcImg[lin(x, max(0,y-1),   w)];

        float g_d = gImg   [lin(x, min(h-1,y+1), w)];
        float u_d = uSrcImg[lin(x, min(h-1,y+1), w)];

        uDstImg[lin(x,y,w)] = (2.0*f + lambda*(g_r*u_r + g_l*u_l + g_u*u_u + g_d*u_d)) /
            (2.0 + lambda*(g_r + g_l + g_u + g_d));
    }
}

int main(int argc, char **argv) {
    cudaDeviceSynchronize();
    CUDA_CHECK;




    // Reading command line parameters:
    // getParam("param", var, argc, argv) looks whether "-param xyz" is specified, and if so stores the value "xyz" in "var"
    // If "-param" is not specified, the value of "var" remains unchanged
    //
    // return value: getParam("param", ...) returns true if "-param" is specified, and false otherwise

#ifdef CAMERA
#else
    // input image
    string image = "";
    bool ret = getParam("i", image, argc, argv);
    if (!ret) cerr << "ERROR: no image specified" << endl;
    if (argc <= 1) { cout << "Usage: " << argv[0] << " -i <image> [-repeats <repeats>] [-gray]" << endl; return 1; }
#endif
    
    // number of computation repetitions to get a better run time measurement
    int repeats = 1;
    getParam("repeats", repeats, argc, argv);
    cout << "repeats: " << repeats << endl;
    
    // load the input image as grayscale if "-gray" is specifed
    bool gray = false;
    getParam("gray", gray, argc, argv);
    cout << "gray: " << gray << endl;

    // Sigma parameter
    float sig = 0.1;
    getParam("sigma", sig, argc, argv);
    cout << "sigma: " << sig << endl;

    // Lambda parameter
    float lambda = 1.0;
    getParam("lambda", lambda, argc, argv);
    cout << "lambda: " << lambda << endl;

    // N parameter
    int N = 1;
    getParam("N", N, argc, argv);
    cout << "N: " << N << endl;

    // Epsilon parameter
    float eps = 0.14;
    getParam("epsilon", eps, argc, argv);
    cout << "epsilon: " << eps << endl;

    // Init camera / Load input image
#ifdef CAMERA

    // Init camera
  	cv::VideoCapture camera(0);
  	if(!camera.isOpened()) { cerr << "ERROR: Could not open camera" << endl; return 1; }
    int camW = 640;
    int camH = 480;
  	camera.set(CV_CAP_PROP_FRAME_WIDTH,camW);
  	camera.set(CV_CAP_PROP_FRAME_HEIGHT,camH);
    // read in first frame to get the dimensions
    cv::Mat mIn;
    camera >> mIn;
    
#else
    
    // Load the input image using opencv (load as grayscale if "gray==true", otherwise as is (may be color or grayscale))
    cv::Mat mIn = cv::imread(image.c_str(), (gray? CV_LOAD_IMAGE_GRAYSCALE : -1));
    // check
    if (mIn.data == NULL) { cerr << "ERROR: Could not load image " << image << endl; return 1; }
    
#endif

    // convert to float representation (opencv loads image values as single bytes by default)
    mIn.convertTo(mIn,CV_32F);
    // convert range of each channel to [0,1] (opencv default is [0,255])
    mIn /= 255.f;
    // get image dimensions
    int w = mIn.cols;         // width
    int h = mIn.rows;         // height
    int nc = mIn.channels();  // number of channels
    cout << "image: " << w << " x " << h << endl;




    // Set the output image format
    cv::Mat mOut(h,w,mIn.type());

    // Allocate raw image arrays
    float *inImg = new float[w*h*nc];
    float *outImg = new float[w*h*mOut.channels()];

    // CUDA Malloc
    float *d_inImg, *d_gImg, *d_u1Img, *d_u2Img, *d_outImg;
    cudaMalloc(&d_inImg,  sizeof(float)*w*h*nc);
    cudaMalloc(&d_gImg,   sizeof(float)*w*h);
    cudaMalloc(&d_u1Img,  sizeof(float)*w*h);
    cudaMalloc(&d_u2Img,  sizeof(float)*w*h);
    cudaMalloc(&d_outImg, sizeof(float)*w*h*nc);
    CUDA_CHECK;

    // For camera mode: Make a loop to read in camera frames
#ifdef CAMERA
    // Read a camera image frame every 30 milliseconds:
    // cv::waitKey(30) waits 30 milliseconds for a keyboard input,
    // returns a value <0 if no key is pressed during this time, returns immediately with a value >=0 if a key is pressed
    while (cv::waitKey(30) < 0)
    {
    // Get camera image
    camera >> mIn;
    // convert to float representation (opencv loads image values as single bytes by default)
    mIn.convertTo(mIn,CV_32F);
    // convert range of each channel to [0,1] (opencv default is [0,255])
    mIn /= 255.f;
#endif

    // Init raw input image array
    addNoise(mIn, sig);
    convert_mat_to_layered (inImg, mIn);






    Timer timer; timer.start();
    
    cudaMemcpy(d_inImg, inImg, w*h*nc*sizeof(float), cudaMemcpyHostToDevice);
    CUDA_CHECK;

    for (int c = 0; c < nc; c++) {
        cudaMemcpy(d_u1Img,  d_inImg + w*h*c, w*h*sizeof(float), cudaMemcpyDeviceToDevice);
        CUDA_CHECK;

        for (int n = 0; n < N; n++) {
            dim3 blockDim(16, 16, 1);
            dim3 gridDim = calcGridDim2d(blockDim, (w+1)/2, h);

            d_g<<<gridDim, blockDim>>>(d_gImg, d_u1Img, w, h, eps, true);
            d_gaussSeidel<<<gridDim, blockDim>>>(d_u2Img, d_u1Img, d_inImg + w*h*c, d_gImg, w, h, lambda, false);
            d_g<<<gridDim, blockDim>>>(d_gImg, d_u1Img, w, h, eps, false);
	        d_gaussSeidel<<<gridDim, blockDim>>>(d_u2Img, d_u1Img, d_inImg + w*h*c, d_gImg, w, h, lambda, true);
            CUDA_CHECK;

            float *d_tmpImg = d_u1Img; d_u1Img = d_u2Img; d_u2Img = d_tmpImg; // Swap u1 and u2 pointers
        }

        cudaMemcpy(d_outImg + w*h*c, d_u1Img, w*h*sizeof(float), cudaMemcpyDeviceToDevice);
        CUDA_CHECK;
    }

    cudaMemcpy(outImg, d_outImg, w*h*nc*sizeof(float), cudaMemcpyDeviceToHost);
    CUDA_CHECK;

    timer.end();  float t = timer.get();  // elapsed time in seconds
    cout << "time: " << t*1000 << " ms" << endl;






    // show input image
    showImage("Input", mIn, 100, 100);  // show at position (x_from_left=100,y_from_above=100)

    // show output image: first convert to interleaved opencv format from the layered raw array
    convert_layered_to_mat(mOut, outImg);
    showImage("Output", mOut, 100+w+40, 100);

    // ### Display your own output images here as needed

#ifdef CAMERA
    // end of camera loop
    }
#else
    // wait for key inputs
    cv::waitKey(0);
#endif

    // Free CUDA buffers
    cudaFree(d_inImg);
    cudaFree(d_gImg);
    cudaFree(d_u1Img);
    cudaFree(d_u2Img);
    cudaFree(d_outImg);
    CUDA_CHECK;


    // save input and result
    cv::imwrite("image_input.png",mIn*255.f);  // "imwrite" assumes channel range [0,255]
    cv::imwrite("image_result.png",mOut*255.f);

    // free allocated arrays
    delete[] inImg;
    delete[] outImg;

    // close all opencv windows
    cvDestroyAllWindows();
    return 0;
}



